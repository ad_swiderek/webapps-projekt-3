##INSTRUKCJA LINUX

Aby wykonywać poniższe operacje w konsoli należy najpierw wpisać komendę sudo su

---

## Pobieranie i uruchamianie backendu 

Aby uruchomić naszą aplikację backendową:

1. Uruchamiamy konsolę i pobieramy repozytorium komendą git clone https://ad_swiderek@bitbucket.org/ad_swiderek/webapps-projekt-3.git
2. Przechodzimy do pobranego katalogu.
3. Wpisujemy komendę apt install npm
4. Wpisujemy komendę nodejs app.js

Od tej chwili nasza aplikacja backendowa działa.
Aby wykonywać połączenia należy uzupełnić pola "login" oraz "password" w pliku app.js.

---

## Pobieranie i uruchamianie frontendu oraz korzystanie z aplikacji

Aby uruchomić naszą aplikację frontendowa, w tle w drugiej konsoli musi działać aplikacja backendowa a następnie:

1. Uruchamiamy konsolę i przechodzimy do katalogu w którym chcemy mieć nasz front.
2. W otwartej konsoli wklejamy: git clone https://bitbucket.org/ad_swiderek/webapps-projekt-2.git
3. Nasze repozytorium zostało pobrane.
4. Przechodzimy do pobranego katalogu.
5. Wpisujemy komendę apt install npm
6. Wpisujemy komendę apt install ng-common
7. Wpisujemy komendę npm start
8. Otwieramy przeglądarkę i wpisujemy http://localhost:4200/
9. Gdy pokokaże się nasza aplikacja, w zaznaczonym na czerwono polu wpisujemy numer telefonu i klikamy "Zadzwoń", następnie powinniśmy uzyskać połączenie

---

## Uruchamianie i obsługa aplikacji PM2

1. Przechodzimy do naszego katalogu pobranego jako backend (Webapps Projekt 3)
2. Wpisujemy komendę apt install npm (o ile nie robiliśmy tego wcześniej)
3. Wpisujemy komendę npm install pm2 -g
4. Uruchamiamy naszą aplikacje pm2 poprzez komendę pm2 start dialer.config.js
5. Aby sprawdzić status uruchomionych aplikacji wpisujemy pm2 status
6. Aby monitorować działanie aplikacji wpisujemy komendę pm2 monit
7. Logi z działania aplikacji znajdują sie w /var/log/dialer.log a konkretnie w pliku o nazwie dialer-0.log

---

## Instalacja i uruchamianie ansible

1. Instlujemy ansible poprzez wpisanie komendy apt install ansible
2. Uruchamiamy konsolę i pobieramy repozytorium komendą git clone https://ad_swiderek@bitbucket.org/ad_swiderek/webapps-projekt-3.git (o ile nie robiliśmy tego wcześniej)
3. Przechodzimy do pobranego katalogu a następnie do katalogu ansible-playbook
4. Uruchamiamy playbook komendą ansible-playbook playbook.yml -i inventory --connection=local -K

Nasz backend jak i frontend zostanie automatycznie pobrany i uruchomiony a aplikacja będzie gotowa do działania na http://localhost:4200/ 
Aby zadzwonić wcześniej trzeba uzupełnić login oraz hasło w repozytorium z backendem.

---

## Uruchamianie chatu

1. Uruchamiamy konsolę i pobieramy repozytorium komendą git clone https://ad_swiderek@bitbucket.org/ad_swiderek/webapps-projekt-3.git (o ile nie robiliśmy tego wcześniej)
2. Wpisujemy komende npm install a następnie npm install --express
3. Uruchamiamy aplikację poprzez wpisanie node app.js

Aby korzystać z chatu należy w przeglądarce wpisać w dwóch oknach: localhost:3000/chat

